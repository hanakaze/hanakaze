#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS 1
#endif
#define FFTW_DLL

#include<cmath>
#include<string>
#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<iostream>
#include<vector>
#include <fftw3.h>
#include<time.h>
#define PI 3.14159265359

using namespace std;

int main() {

	//before run, chech the cutoff k and the output k

	/*Switch Zone begin*/
	double cutoff[4] = {1.0,2.0,5.0,10.0};
	double l;
	printf("Entr lambda value\n");
	cin >> l;
	bool M1, R1, TRho, TRhoWithM;//TRho=1 transform energy density, TRhoWithM=1 transfer with metric
	printf("Mass with measure?\n");
	cin >> M1;
	printf("Radius with measure?\n");
	cin >> R1;
	printf("Transform density?\n");
	cin >> TRho;
	printf("Density with metric?\n");
	cin >> TRhoWithM;
	/*Switch Zone end*/

	FILE * pFile1; //add
	FILE * pFile2;//ans,also used to create mass func
	int i = 0;
	int j = 0;
	int M = 0;//number of data file
	int k = 0;
	int N; //length of array
	double a, b, c, d, e;
	double efr = 0; //effective radius
	int efrin = 0;// effective radius index
	//int Abin = 0; //the largest of A
	long double mass;//max A index
	//const int NL = strlen("test_phi_0.050000_B_0.934464.csv");
	bool flag = true;
	FILE * pFile;
	char filename[255];
	fftw_plan p;
	double *in = NULL;
	fftw_complex *out = NULL;
	long double *fft = NULL;
	long double ftot = 0;
	double h;

	long double centro1 = 0;
	double phi_i, B_i;
	N = 10000000;
	vector<vector<double>> ans(N, vector<double>(5, 0.0));
	char add[255][100];



	sprintf_s(filename, "l_%lf_phi_B.csv", l);
	fopen_s(&pFile1, filename, "r");
	while (fscanf(pFile1, "%s", add[i]) != EOF) {
		i++;
	}
	M = i;
	fclose(pFile1);


	for (k = 3; k < 4; k++){       //the cutoff selection
		sprintf_s(filename, "l_%lf_test_phi_totalCE_withCutoff_%lf.csv", l, cutoff[k]);
		fopen_s(&pFile, filename, "w");
		fclose(pFile);


		for (i = 0; i < M; i++){

			N = 0;
			mass = 0.0;
			efr = 0;
			fopen_s(&pFile2, add[i], "r");
			j = 0;
			while (fscanf(pFile2, "%lf,%lf,%lf,%lf,%lf\n", &a, &b, &c, &d, &e) != EOF){
				ans[j][0] = a;
				ans[j][1] = b;
				ans[j][2] = c;
				ans[j][3] = d;
				ans[j][4] = e;
				j++;
				
			}
			N = j;
			fclose(pFile2);
			h = ans[1][0];


			/*for (j = 0; j <= Abin; j++)
			{
			efr += sqrt(ans[j][1]);
			}
			efrin = (int)ceil(efr);
			efr = efr*1E-3;
			printf("effective radius at %lf, index %d\n", efr, efrin);

			for (j = 0; j <= efrin; j++)
			{
			mass += (pow(j, 2)*((1 + 1 / ans[j][2])*pow(ans[j][4], 2) + (l / 4.)*pow(ans[j][4], 4) + pow(ans[j][3], 2) / ans[j][1]));//sqrt(ans[j][1]*ans[j][2])
			}
			mass = 0.5*mass*1E-9;
			printf("mass is %lf\n", mass);
			//in reduced planck mass Mp^2
			*/

			for (j = 0; j < N; j++)
			{
				if (M1){
					mass += (sqrt(ans[j][1]*ans[j][2])*(pow(j, 2)*((1.0 + 1. / ans[j][2])*pow(ans[j][4], 2) + (l / 4.)*pow(ans[j][4], 4) + pow(ans[j][3], 2) / ans[j][1])));//sqrt(ans[j][1]*ans[j][2])
				}
				else{ mass += ((pow(j, 2)*((1.0 + 1. / ans[j][2])*pow(ans[j][4], 2) + (l / 4.)*pow(ans[j][4], 4) + pow(ans[j][3], 2) / ans[j][1]))); }
			}
			for (j = 0; j < N; j++)
			{
				if (R1){
					efr += (sqrt(ans[j][1] * ans[j][2])*(pow(j, 3)*((1.0 + 1. / ans[j][2])*pow(ans[j][4], 2) + (l / 4.)*pow(ans[j][4], 4) + pow(ans[j][3], 2) / ans[j][1]) / mass));
				}
				else{ efr += (pow(j, 3)*((1.0 + 1. / ans[j][2])*pow(ans[j][4], 2) + (l / 4.)*pow(ans[j][4], 4) + pow(ans[j][3], 2) / ans[j][1]) / mass); }
			}

			efrin = (int)ceil(efr);
			printf("effective radius at %lf, index %d\n", efr, efrin);
			efr = efr*h;
			mass = 0.5*mass*h*h*h;
			printf("mass is %lf\n", mass);

			//in reduced planck mass Mp^2



			N = (int)(cutoff[k]*efrin);
			/*j = 1;
			while (1E3*ans[j][4] - ans[0][4] > 0.0){
				j++;
			}
			N = j;*/
			


			if (in) {
				fftw_free(in);
				in = NULL;
			}
			if (out) {
				fftw_free(out);
				out = NULL;
			}
			if (fft) {
				free(fft);
				fft = NULL;
			}

			in = fftw_alloc_real(N);
			out = fftw_alloc_complex(N);
			fft = (long double *)malloc(N * sizeof(long double));
			p = fftw_plan_dft_r2c_1d(N, in, out, FFTW_MEASURE);

			for (j = 0; j < N; j++) {
				if (TRho)
				{
					if (TRhoWithM){
						in[j] = 2.0*PI*j*h*sqrt(ans[j][1] * ans[j][2])*((1 + 1 / ans[j][2])*pow(ans[j][4], 2) + (l / 4.)*pow(ans[j][4], 4) + pow(ans[j][3], 2) / ans[j][1]);
					}
					else in[j] = 2.0*PI*j*h*((1 + 1 / ans[j][2])*pow(ans[j][4], 2) + (l / 4.)*pow(ans[j][4], 4) + pow(ans[j][3], 2) / ans[j][1]);
				}
				else
				{
					in[j] = 2.0*PI*j*h*ans[j][4];
				}
			}

			fftw_execute(p);
			fftw_destroy_plan(p);
			ftot = 0.0;

			centro1 = 0.0;
			sscanf(add[i], "l_%lf_test_phi_%lf_B_%lf.csv", &l, &phi_i, &B_i);
			sprintf(filename, "l_%lf_test_phi_%.10lf_B_%.10lf_phi_FFT.csv", l, phi_i, B_i);
			fopen_s(&pFile, filename, "w");
			

			for (j = 0; j < N; j++){
				fft[j] = pow(out[j][1], 2) / ((double)j*j);
			}



			/*for (j = 0; j < efrin; j++) {
				ftot += fft[j];
			}*/


			for (j = 2; j < N; j++){
				fprintf(pFile, "%d,%.10lf\n", j, fft[j]);
				fft[j] = fft[j] / fft[1];
			}
			fft[1] = 1.0;
			fclose(pFile);

			for (j = 1; j < N/2; j++){//N/efrin
				if (fft[j]>1E-20) {
					centro1 -= (long double)(j / (N*h))*(long double)(j / (N*h))*(fft[j] * log(fft[j]));
				}
			}
			centro1 = 32.0*pow(3.141592653, 4)*centro1/(N*h);



			sprintf_s(filename, "l_%lf_test_phi_totalCE_withCutoff_%lf.csv", l, cutoff[k]);
			fopen_s(&pFile, filename, "a");
			fprintf(pFile, "%.10lf,%.10lf\n", phi_i, centro1);
			fclose(pFile);

			sprintf_s(filename, "M%dR%d_Rho%dMetric%d_l_%lf_radius_mass.csv", M1, R1,TRho,TRhoWithM,l);
			fopen_s(&pFile, filename, "a");
			fprintf(pFile, "%.10lf,%.10lf,%.10lf,%.10lf\n", phi_i, efr, mass, centro1);
			fclose(pFile);

		}

	}

	system("pause");

}