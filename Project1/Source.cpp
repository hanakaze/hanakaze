#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS 1
#endif

#include<iostream>
#include<fstream>
#include<vector>
#include<cmath>
#include <string>
#include <sstream>  
#include<algorithm>


using namespace std;

double l;

double Ap(double x, double A, double B, double rho, double phi) {
	return pow(A, 2)*x*pow(phi, 2) / B + A*pow(rho, 2)*x + (pow(phi, 2) + l / 4 * pow(phi, 4))*pow(A, 2)*x + A / x - A*A / x;
}

double Bp(double x, double A, double B, double rho, double phi) {
	return B*pow(rho, 2)*x + A*x*pow(phi, 2) - A*B*(pow(phi, 2) + l / 4 * pow(phi, 4))*x - B / x + A*B / x;
}

double rhop(double x, double A, double B, double rho, double phi) {
	return A*(-1 / B + l / 2 * pow(phi, 2) + 1)*phi + (A*(pow(phi, 2) + l / 4 * pow(phi, 4))*x - 1 / x - A / x)*rho;
}

double phip(double rho) {
	return rho;
}

double rhop0(double x, double A, double B, double phi) {
	return A*(-1 / B + l / 2 * pow(phi, 2) + 1)*phi;
}

double rungekutta(double k1, double k2, double k3, double k4) {
	return (k1 + k2 * 2 + k3 * 2 + k4) / 6.0;
}

int main() {
	//before run, check
	//1.data clear switch
	//2.search clients, search length, search step length, search fit
	//3.exit condition

	/*Switch Zone begin*/
	bool cleardata; //switch for clear or not clear data, default to 0
	printf("clear data (1/0)?\n");
	cin >> cleardata;
	printf("Input Lambda\n");
	cin >> l;
	/*Switch Zone end*/

	bool conv = false;
	double a = 0.0, b = 100;
	int N = 100000;
	double h = (b - a) / N;
	vector<vector<double> > ans(N + 1, vector<double>(5, 0.0));
	vector<vector<double> > ansb(N + 1, vector<double>(5, 0.0));
	int Nb = 0;
	double phi_ib, B_ib; //up-to-date best B for phi
	double K1A, K1B, K1rho, K1phi, K2A, K2B, K2rho, K2phi, K3A, K3B, K3rho, K3phi, K4A, K4B, K4rho, K4phi;
	double phi_i;
	int i;
	double B_i;
	double Btar=0.;// the most apporixmate Bi
	//double B[20] = { 0.9344636, 0.868101, 0.801436, 0.735017, 0.669394, 0.605121, 0.542734, 0.482741, 0.425609, 0.371755, 0.321532, 0.275224, 0.233034, 0.195082, 0.161402, 0.131941, 0.106563, 0.085050, 0.067116, 0.052415 };
	FILE * pFile;
	char filename[255];

	if (cleardata){
		sprintf_s(filename, "l_%lf_phi_B.csv", l);
		pFile=fopen( filename, "w");
		fclose(pFile);//flush file name 
		sprintf_s(filename, "l_%lf_phi_B_data.csv", l);
		pFile=fopen( filename, "w");
		fclose(pFile);//flush phi B file
	}

	B_ib = 0.636;
	for (phi_i = 0.222; phi_i <= 0.230; phi_i += 0.001) {
		Nb = 0;
		phi_ib = 0;
	
		//Btar = 1.00026 - 1.22752*pow(phi_i, 1) - 3.75782*pow(phi_i, 2) + 10.2023*pow(phi_i, 3) - 8.99062*pow(phi_i, 4) + 2.80154*pow(phi_i, 5);
		for (B_i = B_ib; B_i >= 0.618;B_i-=1E-7){//(B_i = max(0., Btar - 0.1); B_i <= Btar + 0.1; B_i += 1E-6){
			if (((int)(B_i*1000000)%1000)==0)
				printf("Test %.10lf\n", B_i);
			ans[0][0] = a;

			ans[0][1] = 1.0;
			ans[0][2] = B_i;
			ans[0][3] = 0;
			ans[0][4] = phi_i;

			ans[1][0] = h;
			ans[1][1] = 1.0;
			ans[1][2] = ans[0][2];

			K1rho = h * rhop0(ans[0][0], ans[0][1], ans[0][2], ans[0][4]);
			K1phi = h * phip(ans[0][3]);

			K2rho = h * rhop0(ans[0][0] + h / 2, ans[0][1], ans[0][2], ans[0][4] + K1phi / 2);
			K2phi = h * phip(ans[0][3] + K1rho / 2);

			K3rho = h * rhop0(ans[0][0] + h / 2, ans[0][1], ans[0][2], ans[0][4] + K2phi / 2);
			K3phi = h * phip(ans[0][3] + K2rho / 2);

			K4rho = h * rhop0(ans[0][0] + h, ans[0][1], ans[0][2], ans[0][4] + K3phi / 2);
			K4phi = h * phip(ans[0][3] + K3rho);

			ans[1][3] = ans[0][3] + rungekutta(K1rho, K2rho, K3rho, K4rho);
			ans[1][4] = ans[0][4] + rungekutta(K1phi, K2phi, K3phi, K4phi);

			conv = false;

			for (i = 1; i < N; i++) {
				//K1
				K1A = h * Ap(ans[i][0], ans[i][1], ans[i][2], ans[i][3], ans[i][4]);
				K1B = h * Bp(ans[i][0], ans[i][1], ans[i][2], ans[i][3], ans[i][4]);
				K1rho = h * rhop(ans[i][0], ans[i][1], ans[i][2], ans[i][3], ans[i][4]);
				K1phi = h * phip(ans[i][3]);
				//K2
				K2A = h * Ap(ans[i][0] + h / 2, ans[i][1] + K1A / 2, ans[i][2] + K1B / 2, ans[i][3] + K1rho / 2, ans[i][4] + K1phi / 2);
				K2B = h * Bp(ans[i][0] + h / 2, ans[i][1] + K1A / 2, ans[i][2] + K1B / 2, ans[i][3] + K1rho / 2, ans[i][4] + K1phi / 2);
				K2rho = h * rhop(ans[i][0] + h / 2, ans[i][1] + K1A / 2, ans[i][2] + K1B / 2, ans[i][3] + K1rho / 2, ans[i][4] + K1phi / 2);
				K2phi = h * phip(ans[i][3] + K1rho / 2);
				//K3
				K3A = h * Ap(ans[i][0] + h / 2, ans[i][1] + K2A / 2, ans[i][2] + K2B / 2, ans[i][3] + K2rho / 2, ans[i][4] + K2phi / 2);
				K3B = h * Bp(ans[i][0] + h / 2, ans[i][1] + K2A / 2, ans[i][2] + K2B / 2, ans[i][3] + K2rho / 2, ans[i][4] + K2phi / 2);
				K3rho = h * rhop(ans[i][0] + h / 2, ans[i][1] + K2A / 2, ans[i][2] + K2B / 2, ans[i][3] + K2rho / 2, ans[i][4] + K2phi / 2);
				K3phi = h * phip(ans[i][3] + K2rho / 2);
				//K4
				K4A = h * Ap(ans[i][0] + h, ans[i][1] + K3A, ans[i][2] + K3B, ans[i][3] + K3rho, ans[i][4] + K3phi);
				K4B = h * Bp(ans[i][0] + h, ans[i][1] + K3A, ans[i][2] + K3B, ans[i][3] + K3rho, ans[i][4] + K3phi);
				K4rho = h * rhop(ans[i][0] + h, ans[i][1] + K3A, ans[i][2] + K3B, ans[i][3] + K3rho, ans[i][4] + K3phi);
				K4phi = h * phip(ans[i][3] + K3rho);
				//w
				ans[i + 1][0] = ans[i][0] + h;
				ans[i + 1][1] = ans[i][1] + rungekutta(K1A, K2A, K3A, K4A);
				ans[i + 1][2] = ans[i][2] + rungekutta(K1B, K2B, K3B, K4B);
				ans[i + 1][3] = ans[i][3] + rungekutta(K1rho, K2rho, K3rho, K4rho);
				ans[i + 1][4] = ans[i][4] + rungekutta(K1phi, K2phi, K3phi, K4phi);



				if (ans[i + 1][4] - ans[i][4]>1E-6){//(ans[i + 1][3] >1E-10 || ans[i + 1][4]<-1E-10) {//
					break;
				}

			}

			if (ans[i - 1][4] - ans[i][4] < 1E-10){
				conv = true;
			}

			if (conv&&i>Nb) { //if this solution is ok and it has longer range than before
				Nb = i;
				phi_ib = phi_i;
				B_ib = B_i;
				for (i = 0; i < Nb; i++)
				{
					ansb[i][0] = ans[i][0];
					ansb[i][1] = ans[i][1];
					ansb[i][2] = ans[i][2];
					ansb[i][3] = ans[i][3];
					ansb[i][4] = ans[i][4];

				}

			}
		}

		if (phi_ib > 0) {
			printf("%.10lf Good WITH %.10lf\n", phi_ib, B_ib);


			sprintf_s(filename, "l_%lf_test_phi_%.10lf_B_%.10lf.csv", l, phi_ib, B_ib);//data file name for each phi
			pFile=fopen( filename, "w");
			for (i = 0; i < Nb; i++) {
				if (ansb[i][3]<(1E-10)&&ansb[i][4]>(-1E-10)){
					fprintf(pFile, "%.10lf,%.10lf,%.10lf,%.10lf,%.10lf\n", ansb[i][0], ansb[i][1], ansb[i][2], ansb[i][3], ansb[i][4]);
				}
			}
			fclose(pFile);// data input for each phi


			//data of phi-B 
			sprintf_s(filename, "l_%lf_phi_B.csv", l);
			pFile=fopen( filename, "a");
			fprintf(pFile, "l_%lf_test_phi_%.10lf_B_%.10lf.csv\n", l, phi_ib, B_ib);
			fclose(pFile);//store file name
			sprintf_s(filename, "l_%lf_phi_B_data.csv", l);
			pFile=fopen( filename, "a");
			fprintf(pFile, "%lf,%.10lf\n", l, phi_ib, B_ib);
			fclose(pFile);//store phi,B value
		}
		else {
			printf("No Solution for Phi:%.10lf\n", phi_i);
		}

		
	}
	system("pause");
}