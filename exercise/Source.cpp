#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS 1
#endif
#define FFTW_DLL

#include<cmath>
#include<string>
#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<iostream>
#include<vector>
#include <fftw3.h>
#include<time.h>

using namespace std;

int main() {

	int j = 0;
	int N; //length of array
	double h;
	double ftot = 0;
	FILE * pFile;
	char filename[255];
	fftw_plan p;
	double *in = NULL;
	fftw_complex *out = NULL;
	double *fft = NULL;
	double fftmax;
	long double centro1 = 0;

	N = 5000000;
	h = 0.0001;

	sprintf_s(filename, "FFT_all.csv");
	fopen_s(&pFile, filename, "w");
	fclose(pFile);

	if (in) {
		fftw_free(in);
		in = NULL;
	}
	if (out) {
		fftw_free(out);
		out = NULL;
	}
	if (fft) {
		free(fft);
		fft = NULL;
	}

	in = fftw_alloc_real(N);
	out = fftw_alloc_complex(N);
	fft = (double *)malloc(N * sizeof(double));
	p = fftw_plan_dft_r2c_1d(N, in, out, FFTW_MEASURE);

	for (j = 1; j < N; j++) {
		in[j] = 2*3.1415926*j*h*exp(-pow(j*h, 2));// sin(h*j) / (h*j);// exp(-pow(j*h, 2));
	}
	in[0] = 1.0;


	fftw_execute(p);
	fftw_destroy_plan(p);


	centro1 = 0.0;

	for (j = 1; j < (int)ceil(N / 2.); j++){
		fft[j] = -pow(out[j][1],2)/((double)j*j); //fft[j] = pow(out[j][0], 2) + pow(out[j][1], 2);
		if (j != 0){
			fft[N - j] = fft[j];
			out[N - j][0] = out[j][0];
			out[N-j][1] = -out[j][1];
		}
	}
	if (N % 2 == 0)fft[j] = -pow(out[j][1], 2) / ((double)j*j);//fft[j] = pow(out[j][0], 2) + pow(out[j][1], 2);





	/*fftmax = fft[0];
	for (j = 1; j < N; j++){

		if(fft[j]>fftmax)
			fftmax=fft[j];
	}*/
	fftmax = fft[1];
	for (j = 1; j < N; j++){

		fft[j] = fft[j] / fftmax;
	}


	/*for (j = 0; j < N; j++) {
		ftot += fft[j];
	}


	for (j = 0; j < N; j++)
		fft[j] = fft[j] / ftot;
		*/

	for (j = 1; j < N/2; j++){
		if (fft[j]>1E-20) {
			centro1 -= (long double)(j/(N*h))*(long double)(j/(N*h))*(fft[j] * log(fft[j]));
			printf("cen %i: %lf\n", j, centro1);
		}
	}
	printf("Entropy is: %.10lf\n", 32.0*pow(3.141592653,4)*centro1/(N*h));


	sprintf_s(filename, "FFT_all.csv");
	fopen_s(&pFile, filename, "a");
	for (j = 0; j < N; j++){
		fprintf(pFile, "%.10lf, %.20lf,%.10lf,%.10lf\n",2*3.1415926*j/(N*h), fft[j], out[j][0]/(N*h), out[j][1]/(N*h));
	}
	fclose(pFile);

	
	system("pause");
}