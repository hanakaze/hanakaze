#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS 1
#endif
#define FFTW_DLL

#include<cmath>
#include<string>
#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<iostream>
#include<vector>
#include <fftw3.h>
#include<time.h>

using namespace std;

int main() {

	//before run, chech the cutoff k and the output k

	/*Switch Zone begin*/
	double cutoff[5] = {1.00, 2.00, 3.00, 5.00, 6.00 };
	double l;
	printf("Entr lambda value\n");
	cin >> l;
	bool M1, R1, TRho, TRhoWithM;//TRho=1 transform energy density, TRhoWithM=1 transfer with metric
	printf("Mass with measure?\n");
	cin >> M1;
	printf("Radius with measure?\n");
	cin >> R1;
	printf("Transform density?\n");
	cin >> TRho;
	printf("Density with metric?\n");
	cin >> TRhoWithM;
	/*Switch Zone end*/

	FILE * pFile1; //add
	FILE * pFile2;//ans,also used to create mass func
	int i = 0;
	int j = 0;
	int M = 0;//number of data file
	int k = 0;
	long int N; //length of array
	double a, b, c, d, e;
	double efr = 0; //effective radius
	int efrin = 0;// effective radius index
	//int Abin = 0; //the largest of A
	long double mass;//max A index
	//const int NL = strlen("test_phi_0.050000_B_0.934464.csv");
	bool flag = true;
	FILE * pFile;
	char filename[255];
	fftw_plan p;
	double *in = NULL;
	fftw_complex *out = NULL;
	long double *fft = NULL;
	long double ftot = 0;
	double h;

	long double centro1 = 0;
	double phi_i, B_i;
	N = 1000000;
	vector<vector<double>> ans(N, vector<double>(5, 0.0));
	char add[255][100];



	sprintf_s(filename, "l_%lf_phi_B.csv", l);
	fopen_s(&pFile1, filename, "r");
	while (fscanf(pFile1, "%s", add[i]) != EOF) {
		i++;
	}
	M = i;
	fclose(pFile1);



	/*sprintf_s(filename, "l_%lf_phi_i_ModifiedCE.csv", l);
	fopen_s(&pFile, filename, "w");
	fclose(pFile);


	sprintf_s(filename, "l_%lf_reff_ModifiedCE.csv", l);
	fopen_s(&pFile, filename, "w");
	fclose(pFile);*/

	for (k = 0; k < 5; k++){       //the cutoff selection
		sprintf_s(filename, "l_%lf_test_phi_totalCE_withCutoff_%lf.csv", l, cutoff[k]);
		fopen_s(&pFile, filename, "w");
		fclose(pFile);


		for (i = 0; i < M; i++){

			N = 0;
			mass = 0.0;
			efr = 0;
			fopen_s(&pFile2, add[i], "r");
			j = 0;
			while (fscanf(pFile2, "%lf,%lf,%lf,%lf,%lf\n", &a, &b, &c, &d, &e) != EOF){
				ans[j][0] = a;
				ans[j][1] = b;
				ans[j][2] = c;
				ans[j][3] = d;
				ans[j][4] = e;
				j++;

			}
			fclose(pFile2);
			h = ans[1][0];
			N = j;


			/*for (j = 0; j <= Abin; j++)
			{
			efr += sqrt(ans[j][1]);
			}
			efrin = (int)ceil(efr);
			efr = efr*1E-3;
			printf("effective radius at %lf, index %d\n", efr, efrin);

			for (j = 0; j <= efrin; j++)
			{
			mass += (pow(j, 2)*((1 + 1 / ans[j][2])*pow(ans[j][4], 2) + (l / 4.)*pow(ans[j][4], 4) + pow(ans[j][3], 2) / ans[j][1]));//sqrt(ans[j][1]*ans[j][2])
			}
			mass = 0.5*mass*1E-9;
			printf("mass is %lf\n", mass);
			//in reduced planck mass Mp^2
			*/

			for (j = 0; j < N; j++)
			{
				if (M1){
					mass += (sqrt(ans[j][1])*(pow(j, 2)*((1.0 + 1. / ans[j][2])*pow(ans[j][4], 2) + (l / 4.)*pow(ans[j][4], 4) + pow(ans[j][3], 2) / ans[j][1])));//sqrt(ans[j][1]*ans[j][2])
				}
				else{ mass += ((pow(j, 2)*((1.0 + 1. / ans[j][2])*pow(ans[j][4], 2) + (l / 4.)*pow(ans[j][4], 4) + pow(ans[j][3], 2) / ans[j][1]))); }
			}
			for (j = 10; j < N-10; j++)
			{
				if (1E2*ans[j][4]- ans[0][4]< 0.0)
				{
					efrin = j;
					break;
				}
			}

			//efrin = (int)ceil(efr);
			
			efr = efrin*h;
			printf("effective radius at %lf, index %d\n", efr, efrin);
			mass = 0.5*mass*h*h*h;
			printf("mass is %lf\n", mass);

			//in reduced planck mass Mp^2

			N = (int)(cutoff[k] * efrin);


			if (in) {
				fftw_free(in);
				in = NULL;
			}
			if (out) {
				fftw_free(out);
				out = NULL;
			}
			if (fft) {
				free(fft);
				fft = NULL;
			}

			in = fftw_alloc_real(N);
			out = fftw_alloc_complex(N);
			fft = (long double *)malloc(N * sizeof(long double));
			p = fftw_plan_dft_r2c_1d(N, in, out, FFTW_MEASURE);

			for (j = 0; j < N; j++) {
				if (TRho)
				{
					if (TRhoWithM){
						in[j] = 1E3*sqrt(ans[j][1])*((1 + 1 / ans[j][2])*pow(ans[j][4], 2) + (l / 4.)*pow(ans[j][4], 4) + pow(ans[j][3], 2) / ans[j][1]);
					}
					else in[j] = 1E3*((1 + 1 / ans[j][2])*pow(ans[j][4], 2) + (l / 4.)*pow(ans[j][4], 4) + pow(ans[j][3], 2) / ans[j][1]);
				}
				else
				{
					in[j] = 1E3*ans[j][4];
				}
			}

			fftw_execute(p);
			fftw_destroy_plan(p);
			ftot = 0.0;

			centro1 = 0.0;
			sscanf(add[i], "l_%lf_test_phi_%lf_B_%lf.csv", &l, &phi_i, &B_i);
			sprintf(filename, "l_%lf_test_phi_%.10lf_B_%.10lf_phi_FFT.csv", l, phi_i, B_i);
			fopen_s(&pFile, filename, "w");

			for (j = 0; j < (int)ceil(N / 2.); j++){
				fft[j] = pow(out[j][0], 2) + pow(out[j][1], 2);
				if (j != 0){
					fft[N - j] = fft[j];
				}
			}
			if (N % 2 == 0)fft[j] = pow(out[j][0], 2) + pow(out[j][1], 2);
		



			for (j = 0; j < N; j++) {
				ftot += fft[j];
			}


			for (j = 0; j < N; j++){
				fft[j] = fft[j] / ftot;
				fprintf(pFile, "%d,%.10lf\n", j, fft[j]);
			}
			fclose(pFile);

			for (j = (int)(N/(2*efrin)); j < N; j++){
				if (fft[j]) {
					centro1 -= (fft[j] * log(fft[j]));
				}
			}



			sprintf_s(filename, "l_%lf_test_phi_totalCE_withCutoff_%lf.csv", l, cutoff[k]);
			fopen_s(&pFile, filename, "a");
			fprintf(pFile, "%.10lf,%.10lf\n", phi_i, centro1);
			fclose(pFile);

			if (cutoff[k] == 1)
			{
				sprintf_s(filename, "M%dR%d_Rho%dMetric%d_l_%lf_radius_mass.csv", M1, R1, TRho, TRhoWithM, l);
				fopen_s(&pFile, filename, "a");
				fprintf(pFile, "%.10lf,%.10lf,%.10lf,%.10lf\n", phi_i, efr, mass, centro1);
				fclose(pFile);
			}

			/*sprintf_s(filename, "l_%lf_phi_i_ModifiedCE.csv", l);
			fopen_s(&pFile, filename, "a");
			fprintf(pFile, "%lf,%.10lf\n", phi_i, centro1 / mass);
			fclose(pFile);*/

			/*sprintf_s(filename, "l_%lf_reff_ModifiedCE.csv", l);
			fopen_s(&pFile, filename, "a");
			fprintf(pFile, "%lf,%.10lf\n", efr*efr, centro1);
			fclose(pFile);*/

		}

	}

	system("pause");

}